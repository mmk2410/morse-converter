package de.marcelkapfer.c.morseconverter.intelligentCodeRecognization;

/*
    This is a Java application for converting writtenMorse and normal morse code.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */

/**
 * Marcel Kapfer (mmk2410) <marcelmichaelkapfer@yahoo.co.nz>
 * Licensed under
 */
public class NormalMorseCodeRecognization {

    public static Boolean isCode(String input){
        input = input.toLowerCase();
        return !(input.contains("a") || input.contains("b") ||
                input.contains("c") || input.contains("d") ||
                input.contains("e") || input.contains("f") ||
                input.contains("g") || input.contains("h") ||
                input.contains("i") || input.contains("j") ||
                input.contains("k") || input.contains("l") ||
                input.contains("m") || input.contains("n") ||
                input.contains("o") || input.contains("p") ||
                input.contains("q") || input.contains("r") ||
                input.contains("s") || input.contains("t") ||
                input.contains("u") || input.contains("v") ||
                input.contains("w") || input.contains("x") ||
                input.contains("y") || input.contains("z") ||
                input.contains("0") || input.contains("1") ||
                input.contains("2") || input.contains("3") ||
                input.contains("4") || input.contains("5") ||
                input.contains("6") || input.contains("7") ||
                input.contains("8") || input.contains("9") ||
                input.contains("Ä") || input.contains("Ö") ||
                input.contains("Ü") || input.contains("ß") ||
                input.contains("$") || input.contains(",") ||
                input.contains(":") || input.contains(";") ||
                input.contains("!") || input.contains("?") ||
                input.contains("+") || input.contains("_") ||
                input.contains("(") || input.contains(")") ||
                input.contains("=") || input.contains("/") ||
                input.contains("@") || input.contains("'"));
    }

}
