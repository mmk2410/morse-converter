package de.marcelkapfer.c.morseconverter;

/*
    This is a Java application for converting writtenMorse and normal morse code.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */

import de.marcelkapfer.c.morseconverter.engine.DecodeNormalMorseManager;
import de.marcelkapfer.c.morseconverter.engine.DecodeWrittenMorseManager;
import de.marcelkapfer.c.morseconverter.engine.EncodeNormalMorseManager;
import de.marcelkapfer.c.morseconverter.engine.EncodeWrittenMorseManager;
import de.marcelkapfer.c.morseconverter.intelligentCodeRecognization.NormalMorseCodeRecognization;
import de.marcelkapfer.c.morseconverter.intelligentCodeRecognization.WrittenMorseCodeRecognization;
import de.marcelkapfer.c.morseconverter.update.Update;
import de.marcelkapfer.c.morseconverter.update.UpdateDialog;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.io.IOException;
import java.util.ResourceBundle;

public class Main extends JFrame {

    // Version numbers for update check
    public static final int currentVersion = 200;
    public static int latestVersion = currentVersion;

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("de.marcelkapfer.c.morseconverter.text");
    private JTabbedPane morseConverterPane;
    private JPanel panel1;
    private JTextArea inputAreaWrittenMorse;
    private JTextArea outputAreaWrittenMorse;
    private JTextArea inputAreaNormalMorse;
    private JTextArea outputAreaNormalMorse;
    private JButton updateDialogButton;
    private JLabel updateAvailable;
    private JTextPane aboutText;

    // Contains mainly listeners
    public Main() {
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
        HTMLDocument htmlDocument = (HTMLDocument) htmlEditorKit.createDefaultDocument();
        Element element = htmlDocument.getDefaultRootElement();
        try {
            htmlDocument.setInnerHTML(element, resourceBundle.getString("about"));
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
        aboutText.setDocument(htmlDocument);
        // Listener for the writtenMorse input field
        inputAreaWrittenMorse.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                instantWrittenMorseConvert();
            }
        });
        // Listener for the normal Morse input field
        inputAreaNormalMorse.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                instantNormalMorseConvert();
            }
        });

        // Trying to read a file from a server
        latestVersion = Update.getLatestVersion("http://lab.marcel-kapfer.de/writtenmorse/desktop/version.txt", currentVersion);

        if (currentVersion < latestVersion){
            JOptionPane.showMessageDialog(null, resourceBundle.getString("updateAvailable"));
            updateAvailable.setVisible(true);
            updateDialogButton.setText(resourceBundle.getString("aboutUpdateAvailableButton"));
        }

        updateDialogButton.addActionListener(e -> updateDialog());

        // Mouse listener for selecting text on left click in output text area
        outputAreaWrittenMorse.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    outputAreaWrittenMorse.requestFocus();
                    outputAreaWrittenMorse.selectAll();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        outputAreaNormalMorse.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    outputAreaNormalMorse.requestFocus();
                    outputAreaNormalMorse.selectAll();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // Popup Menus

        // Popup Menu for writtenMorse input text field

        JPopupMenu wInputPopup = new JPopupMenu();
        addPopup(inputAreaWrittenMorse, wInputPopup);

        // Item for paste
        JMenuItem pasteWInput = new JMenuItem(resourceBundle.getString("paste"));
        pasteWInput.addActionListener(e -> {
            onPaste(inputAreaWrittenMorse);
            instantWrittenMorseConvert();
        });
        wInputPopup.add(pasteWInput);

        // Item for clear
        JMenuItem clearWInput = new JMenuItem(resourceBundle.getString("clear"));
        clearWInput.addActionListener(e -> clear(inputAreaWrittenMorse));
        wInputPopup.add(clearWInput);

        // Popup Menu for writtenMorse output text field
        JPopupMenu wOutputPopup = new JPopupMenu();
        addPopup(outputAreaWrittenMorse, wOutputPopup);

        // Item for copy
        JMenuItem copyWOutput = new JMenuItem(resourceBundle.getString("copy"));
        copyWOutput.addActionListener(e -> onCopy(outputAreaWrittenMorse));
        wOutputPopup.add(copyWOutput);


        // Popups for Normal Morse

        // Popup for Normal Morse input text field
        JPopupMenu nInputMenu = new JPopupMenu();
        addPopup(inputAreaNormalMorse, nInputMenu);

        // Item for paste
        JMenuItem nPasteInput = new JMenuItem(resourceBundle.getString("paste"));
        nPasteInput.addActionListener(e -> {
            onPaste(inputAreaNormalMorse);
            instantNormalMorseConvert();
        });
        nInputMenu.add(nPasteInput);

        // Item for clear
        JMenuItem nClearInput = new JMenuItem(resourceBundle.getString("clear"));
        nClearInput.addActionListener(e -> clear(inputAreaNormalMorse));
        nInputMenu.add(nClearInput);

        // Popup for output text field

        JPopupMenu nOutputMenu = new JPopupMenu();
        addPopup(outputAreaNormalMorse, nOutputMenu);

        // Menu item for copy
        JMenuItem nCopyOutput = new JMenuItem(resourceBundle.getString("copy"));
        nCopyOutput.addActionListener(e -> onCopy(outputAreaNormalMorse));
        nOutputMenu.add(nCopyOutput);

    }
    // The holy main method
    public static void main(String[] args){
        // Tries to present the app in an native look
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e){
            e.printStackTrace();
        }
        // Starting the application
        JFrame main = new JFrame("Main");
        main.setContentPane(new Main().morseConverterPane);
        main.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // Title
        main.setTitle(resourceBundle.getString("title"));
        main.pack();
        // Window position and Size
        main.setBounds(100, 100, 800, 400);
        main.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/res/icon-xxxhdpi.png")));
        // MAKE IT HAPPEN :D
        main.setVisible(true);
        System.out.print(System.lineSeparator());
    }

    // Method for converting writtenMorse
    private void instantWrittenMorseConvert(){
        // Gets the text from the input field
        String input = inputAreaWrittenMorse.getText();
        // checks if the code is a writtenMorse code
        if(WrittenMorseCodeRecognization.isCode(input)) {
            // converts the code into normal letters
            outputAreaWrittenMorse.setText(EncodeWrittenMorseManager.getEncodedString(input));
        } else {
            // converts the code into writtenMorse
            outputAreaWrittenMorse.setText(DecodeWrittenMorseManager.getDecodedString(input));
        }
    }

    // Method for converting normal Morse
    private void instantNormalMorseConvert(){
        // Gets the text from the input field
        String input = inputAreaNormalMorse.getText();
        // checks if the code is a normal Morse code
        if(NormalMorseCodeRecognization.isCode(input)){
            // converts the code into normal letters
            outputAreaNormalMorse.setText(EncodeNormalMorseManager.getEncodedString(input));
        } else {
            // converts the code into writtenMorse
            outputAreaNormalMorse.setText(DecodeNormalMorseManager.getDecodedString(input));
        }
    }

    private void updateDialog() {
        UpdateDialog.main(null);
    }

    private static void addPopup(Component component, final JPopupMenu popup) {
        component.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showMenu(e);
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    showMenu(e);
                }
            }

            private void showMenu(MouseEvent e) {
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        });
    }

    /*
     * Past function
     */
    private void onPaste(JTextArea tf) {
        try {
            Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
            Transferable t = c.getContents(this);
            if (t == null)
                return;
            try {
                tf.setText((String) t.getTransferData(DataFlavor.stringFlavor));
            } catch (Exception e) {
                e.printStackTrace();
            }// try
        } catch (Exception e) {
            e.printStackTrace();
        }
    }// onPaste

    /*
     * Copy function
     */
    private void onCopy(JTextArea tf) {
        StringSelection selection = new StringSelection(tf.getText());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }

    private void clear(JTextArea jTextArea) {
        jTextArea.setText("");
    }
}
