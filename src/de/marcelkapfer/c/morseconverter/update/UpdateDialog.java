package de.marcelkapfer.c.morseconverter.update;

/*
    This is a Java application for converting writtenMorse and normal morse code.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */


import de.marcelkapfer.c.morseconverter.Main;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ResourceBundle;

public class UpdateDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel updateDialogQuestion;
    private JLabel updateDialogNewVersion;
    private final ResourceBundle resourceBundle = ResourceBundle.getBundle("de.marcelkapfer.c.morseconverter.text");

    private UpdateDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        if(Main.currentVersion == Main.latestVersion){
            buttonOK.setVisible(false);
            buttonCancel.setText(resourceBundle.getString("updateDialogClose"));
            updateDialogNewVersion.setVisible(false);
            updateDialogQuestion.setText(resourceBundle.getString("updateDialogUpToDateText"));
        }
        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        try {
            String path = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            path = path.substring(0, path.lastIndexOf("/"));
            if(Update.isConnected("http://lab.marcel-kapfer.de/writtenmorse/desktop/morseconverter.jar")){
                if(Update.update(path +  File.separator, "morseconverter.jar")){
                    JOptionPane.showMessageDialog(null, resourceBundle.getString("update-successful"));
                } else {
                    JOptionPane.showMessageDialog(null, resourceBundle.getString("update-not-successful"));
                }
            } else {
                JOptionPane.showMessageDialog(null, resourceBundle.getString("update-no-connection"));
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, resourceBundle.getString("update-error"));
        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        UpdateDialog dialog = new UpdateDialog();
        dialog.pack();
        dialog.setVisible(true);
    }

}
