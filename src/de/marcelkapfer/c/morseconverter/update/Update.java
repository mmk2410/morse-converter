package de.marcelkapfer.c.morseconverter.update;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/*
    This is a class for updating a application.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */

public class Update {

    private static URL update;

    /**
     *
     * Checks, if a connection to the server is possible
     *
     * Argument: The URL of the file with the version number as a string
     *
     */
    public static boolean isConnected(@SuppressWarnings("SameParameterValue") String url){
        try {
            update = new URL(url);
            update.openStream();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * Updates the application by overwriting the existing .jar file
     *
     * Arguments: The path where the jar file is located, the name of the jar file
     *
     */
    public static boolean update(String path, @SuppressWarnings("SameParameterValue") String filename){
        try {
            ReadableByteChannel updateChannel = Channels.newChannel(update.openStream());
            FileOutputStream updateOutputStream = new FileOutputStream(path + filename);
            updateOutputStream.getChannel().transferFrom(updateChannel, 0, Long.MAX_VALUE);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     *
     * Get the version number of the latest version.
     *
     * Argument: The URL of the version file as a string
     *
     * It is highly recommended to give also the version number of the current version as an argument
     *
     */
    @SuppressWarnings("unused")
    public static int getLatestVersion(String versionFile){
        return getLatestVersion(versionFile, 0);
    }

    /**
     *
     * Get the version number of the latest version.
     *
     * Arguments: The URL of the version file as a string, the current version number
     *
     * The current version number is used as a backup if getting the actual version number fails.
     *
     */
    public static int getLatestVersion(String versionFile, int currentVersion){
        int latestVersion = currentVersion;
        try {
            URL versionURL = new URL(versionFile);
            BufferedReader versionReader = new BufferedReader(new InputStreamReader(versionURL.openStream()));
            latestVersion = Integer.valueOf(versionReader.readLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return latestVersion;
    }

}
