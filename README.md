# Morse Converter (Desktop Client)

A writtenMorse and normal Morse code converter.
A detailed description about this program can be found on the [GitLab Wiki](https://gitlab.com/mmk2410/writtenmorse-specs/wikis/Home).

## Prerequisites

 * 2.0 or later: Java 8 or higher
 - 2.0 or earlier: Java 6 or higher

## Contributing


1. Fork it
2. Create a feature branch with a meaningful name (`git checkout -b my-new-feature`)
3. Add yourself to the CONTRIBUTORS file
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to your branch (`git push origin my-new-feature`)
6. Create a new merge request

If you have any questions feel free to write me a mail at [me@mmk2410.org](mailto:me@mmk2410.org) or contact me on [Twitter](https://twitter.com/mmk2410). If your question is more general, don't hesitate to create an issue.

## Packages

### Linux

#### Ubuntu

I created an Ubuntu repository for the app. You can use it by running the following lines:

```
sudo apt-add-repository ppa:mmk2410/morse-converter

sudo apt-get update

sudo apt-get install morse-converter
```

View this package repository at [Launchpad](https://launchpad.net/~mmk2410/+archive/ubuntu/morse-converter).

#### Debian

You can use the Ubuntu packages (or even the repository) with Debian.
